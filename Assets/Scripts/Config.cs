﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Config
{
    public static int HighScore
    {
        set
        {
            PlayerPrefs.SetInt("HighScore", value);
        }
        get
        {
            return PlayerPrefs.GetInt("HighScore", 0);
        }
    }

    public static int FoodType
    {
        get
        {
            int type = PlayerPrefs.GetInt("FoodType", -1);
            if (type < 0)
            {
                type = Random.Range(0, 4);
                PlayerPrefs.SetInt("FoodType", type);
            }
            return type;
        }
    }

    public static int DNAPoints
    {
        set
        {
            PlayerPrefs.SetInt("DNAPoints", value);
        }
        get
        {
            return PlayerPrefs.GetInt("DNAPoints", 0);
        }
    }

    public static int PointsLevel
    {
        set
        {
            PlayerPrefs.SetInt("PointsLevel", value);
        }
        get
        {
            return PlayerPrefs.GetInt("PointsLevel", 0);
        }
    }

    public static int DNAPointsLevel
    {
        set
        {
            PlayerPrefs.SetInt("DNAPointsLevel", value);
        }
        get
        {
            return PlayerPrefs.GetInt("DNAPointsLevel", 0);
        }
    }

    public static int Points()
    {
        return 10 + PointsLevel;
    }

    public static int PointsDNA(int points)
    {
        return (int)((float)points / 100f) * (DNAPointsLevel + 1);
    }

    public static int PointsUpgradeCost()
    {
        return (PointsLevel + 1) * 15;
    }

    public static int DNAUpgradeCost()
    {
        return (DNAPointsLevel + 1) * 50;
    }
}
