﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodReadyField : MonoBehaviour
{
    private GameController gameController;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Food"))
        {
            gameController.FoodReady();
        }
    }

    public void AttachGameController(GameController controller)
    {
        gameController = controller;
    }
}
