﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    private float spawnOffset;
    private GameController gameController;

    [SerializeField] private List<Food> foodPrefabs;
    [SerializeField] private List<Transform> foodPool;
    [SerializeField] private float maxSpawnOffset;
    [SerializeField] private float spawnMultiplier;
    
    public void StartSpawn()
    {
        spawnOffset = maxSpawnOffset;
        StartCoroutine(SpawnCoroutine());
    }

    public void StopSpawn()
    {
        StopAllCoroutines();
    }

    public void AttachGameController(GameController newGameController)
    {
        gameController = newGameController;
    }

    private IEnumerator SpawnCoroutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(spawnOffset);

            var newFood = GameObject.Instantiate(foodPrefabs[RandFood()], transform);
            gameController?.AddFood(newFood);
            spawnOffset -= spawnMultiplier;
            if(spawnOffset < 0.35f)
            {
                spawnOffset = 0.35f;
            }
        }
    }

    private int RandFood()
    {
        return Random.Range(0, foodPrefabs.Count);
    }
}
