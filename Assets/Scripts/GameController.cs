﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private enum MoveToEnum
    {
        Right,
        Left
    }

    private enum State
    {
        Game,
        Menu
    }

    private Vector2 touchPosition;
    private Queue<Food> foodList;
    private State state;

    public int points;

    [SerializeField] private FoodSpawner foodSpawner;
    [SerializeField] private Creature creature;
    [SerializeField] private Trash trash;
    [SerializeField] private LooseField looseField;
    [SerializeField] private FoodReadyField foodReadyField;
    [SerializeField] private GameUI gameUI;

    private void Awake()
    {
        foodList = new Queue<Food>();
    }

    void Start ()
    {
        foodSpawner.AttachGameController(this);
        creature.AttachGameController(this);
        trash.AttachGameController(this);
        trash.AttachCreature(creature);
        looseField.AttachGameController(this);
        foodReadyField.AttachGameController(this);

        state = State.Menu;

        gameUI.UpdateHighScore(Config.HighScore);
        gameUI.UpdateDnaPoints(Config.DNAPoints);
    }

    private void Update()
    {
        TouchControl();
        MouseControl();
    }

    public void StartGame()
    {
        creature.Init();
        gameUI.HideTutorial();
        gameUI.HideButtons();
        state = State.Game;
        foodSpawner.StartSpawn();
        points = 0;
        gameUI.UpdateScore(points);
    }

    public void Loose()
    {
        foodSpawner.StopSpawn();
        state = State.Menu;
        gameUI.ShowTutorial();
        gameUI.ShowButtons();
        Config.DNAPoints += Config.PointsDNA(points);
        gameUI.UpdateDnaPoints(Config.DNAPoints);

        while (foodList.Count > 0)
        {
            Destroy(foodList.Peek().gameObject);
            foodList.Dequeue();
        }
    }

    public void AddFood(Food newFood)
    {
        foodList.Enqueue(newFood);
    }

    public void AddPoints(int points)
    {
        this.points += points;
        gameUI.UpdateScore(this.points);

        if(this.points > Config.HighScore)
        {
            gameUI.UpdateHighScore(this.points);
            Config.HighScore = this.points;
        }
    }

    public void FoodReady()
    {
        if (foodList.Count > 0)
        {
            foodList.Peek().Ready();
        }
    }

    private void DestroyFood(Vector2 dir)
    {
        if(dir.x >= 0.8f)
        {
            MoveFoodTo(MoveToEnum.Right);
        }

        if (dir.x <= -0.8f)
        {
            MoveFoodTo(MoveToEnum.Left);
        }
    }

    private void MoveFoodTo(MoveToEnum move)
    {
        if (foodList.Count <= 0)
            return;

        while (foodList.Peek() == null && foodList.Count > 0)
        {
            foodList.Dequeue();
        }
        
        if (foodList.Peek().FoodState == Food.State.Ready)
        {
            if (move == MoveToEnum.Left)
            {
                foodList.Peek().Left();
            }
            else
            {
                foodList.Peek().Right();
            }
            foodList.Dequeue();
        }
    }

    private void MouseControl()
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector2 pos = Input.mousePosition;
            DestroyFood((pos - touchPosition).normalized);
        }
    }

    private void TouchControl()
    {
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Began)
            {
                touchPosition = touch.position;
            }

            if(touch.phase == TouchPhase.Ended)
            {
                Vector2 pos = touch.position;
                DestroyFood((pos - touchPosition).normalized);
            }
        }
    }
}
