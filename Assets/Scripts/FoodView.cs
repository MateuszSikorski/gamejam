﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodView : MonoBehaviour
{
    [SerializeField] private Sprite[] foodSprites;
    [SerializeField] private Image foodImage;

    public void UpdateFood(int index)
    {
        foodImage.sprite = foodSprites[index];
    }
}
