﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUI : MonoBehaviour
{
    [SerializeField] private PointsView dnaPoints;
    [SerializeField] private GameObject startGameButton;
    [SerializeField] private GameObject shopButton;
    [SerializeField] private ShopController shop;
    [SerializeField] private FoodView foodView;

    public void UpdateDNAPoints()
    {
        dnaPoints.UpdatePoints(Config.DNAPoints);
    }

    public void UpdateFoodType()
    {
        foodView.UpdateFood(Config.FoodType);
    }

    public void ShowMenu()
    {
        startGameButton.SetActive(true);
        shopButton.SetActive(true);
        shop.gameObject.SetActive(false);
    }

    public void ShowShop()
    {
        startGameButton.SetActive(false);
        shopButton.SetActive(false);
        shop.gameObject.SetActive(true);
        shop.Init();
    }
}
