﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelView : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI costText;
    [SerializeField] private TMPro.TextMeshProUGUI levelText;

    public void UpdateLevel(int level)
    {
        levelText.text = level.ToString();
    }

    public void UpdateCost(int cost)
    {
        costText.text = cost.ToString();
    }
}
