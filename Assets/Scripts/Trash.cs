﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{
    private GameController gameController;
    private Creature creature;

    public void AttachGameController(GameController controller)
    {
        gameController = controller;
    }

    public void AttachCreature(Creature creture)
    {
        this.creature = creture;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Food"))
        {
            var food = collision.gameObject.GetComponent<Food>();
            if (food.FoodType == creature.FoodType)
            {
                gameController.Loose();
            }
            else
            {
                gameController.AddPoints(Config.PointsLevel);
            }
        }
    }
}
