﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    [SerializeField] private PointsView score;
    [SerializeField] private PointsView highScore;
    [SerializeField] private PointsView dnaPoints;
    [SerializeField] private GameObject tutorial;
    [SerializeField] private GameObject startGameButton;
    [SerializeField] private GameObject menuButton;

    public void UpdateScore(int points)
    {
        score.UpdatePoints(points);
    }

    public void UpdateHighScore(int points)
    {
        highScore.UpdatePoints(points);
    }

    public void UpdateDnaPoints(int points)
    {
        dnaPoints.UpdatePoints(points);
    }

    public void ShowTutorial()
    {
        tutorial.SetActive(true);
    }

    public void HideTutorial()
    {
        tutorial.SetActive(false);
    }

    public void ShowButtons()
    {
        startGameButton.SetActive(true);
        menuButton.SetActive(true);
    }

    public void HideButtons()
    {
        startGameButton.SetActive(false);
        menuButton.SetActive(false);
    }
}
