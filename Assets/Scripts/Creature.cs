﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour
{
    private GameController gameController;
    private Food.Type foodType;

    public Food.Type FoodType => foodType;
    
    public void Init()
    {
        foodType = (Food.Type)Config.FoodType;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Food"))
        {
            var food = collision.gameObject.GetComponent<Food>();
            if (food.FoodType == foodType)
            {
                gameController.AddPoints(Config.Points());
            }
            else
            {
                gameController.Loose();
            }
        }
    }

    public void AttachGameController(GameController newGameController)
    {
        gameController = newGameController;
    }
}
