﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [SerializeField] private GameController gameController;
    [SerializeField] private GameObject gameUI;
    [SerializeField] private MenuUI menuUI;
    [SerializeField] private GameObject creature;

    private void Start()
    {
        menuUI.UpdateDNAPoints();
        menuUI.UpdateFoodType();
    }

    public void StartGame()
    {
        menuUI.gameObject.SetActive(false);
        gameUI.SetActive(true);
        gameController.gameObject.SetActive(true);
        creature.SetActive(false);
    }

    public void GoToMenu()
    {
        menuUI.gameObject.SetActive(true);
        gameUI.SetActive(false);
        gameController.gameObject.SetActive(false);
        menuUI.UpdateDNAPoints();
        menuUI.UpdateFoodType();
        creature.SetActive(true);
    }
}
