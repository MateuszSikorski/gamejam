﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour
{
    [SerializeField] private LevelView pointslevel;
    [SerializeField] private LevelView dnaLevel;
    [SerializeField] private PointsView dnaPoints;

    public void Init()
    {
        pointslevel.UpdateLevel(Config.PointsLevel);
        pointslevel.UpdateCost(Config.PointsUpgradeCost());

        dnaLevel.UpdateLevel(Config.DNAPointsLevel);
        dnaLevel.UpdateCost(Config.DNAUpgradeCost());

        dnaPoints.UpdatePoints(Config.DNAPoints);
    }

    public void UpgradeDNA()
    {
        int cost = Config.DNAUpgradeCost();
        if (Config.DNAPoints >= cost)
        {
            Config.DNAPoints -= cost;
            Config.DNAPointsLevel++;
        }

        Init();
    }

    public void UpgradePoints()
    {
        int cost = Config.PointsUpgradeCost();
        if (Config.DNAPoints >= cost)
        {
            Config.DNAPoints -= cost;
            Config.PointsLevel++;
        }

        Init();
    }
}
