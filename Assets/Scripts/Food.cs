﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public enum State
    {
        Ready,
        Falling,
        Right,
        Left
    }

    public enum Type
    {
        Type1 = 0,
        Type2 = 1,
        Type3 = 2,
        Type4 = 3,
        Type5 = 4,
        Type6 = 5,
        Type7 = 6
    }

    private State state;

    [SerializeField] private Type type;
    [SerializeField] private float fallingSpeed;

    public State FoodState => state;
    public Type FoodType => type;

    private void Awake()
    {
        state = State.Falling;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("FoodReadyField"))
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (state == State.Falling || state == State.Ready)
        {
            transform.position += Vector3.down * fallingSpeed * Time.deltaTime;
        }
        else if(state == State.Right)
        {
            transform.position += Vector3.right * fallingSpeed * Time.deltaTime * 5f;
        }
        else if (state == State.Left)
        {
            transform.position -= Vector3.right * fallingSpeed * Time.deltaTime * 5f;
        }
    }

    public void Ready()
    {
        state = State.Ready;
    }

    public void Left()
    {
        state = State.Left;
    }
    public void Right()
    {
        state = State.Right;
    }
}
