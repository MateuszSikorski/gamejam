﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsView : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI pointsText;

    public void UpdatePoints(int points)
    {
        pointsText.text = points.ToString();
    }
}
